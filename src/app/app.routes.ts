import { Routes } from '@angular/router';
import { CandidatesSliderComponent } from './candidates-slider/candidates-slider.component';
import { CandidatesComponent } from './candidates/candidates.component';
import { AppComponent } from './app.component';

export const routes: Routes = [
    // {path: '', redirectTo: '', pathMatch: 'full'},
    {path: '', component: CandidatesComponent},
    {path: 'current-district/:current-district', component: CandidatesSliderComponent}
];
