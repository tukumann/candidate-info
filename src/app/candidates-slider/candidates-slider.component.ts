import { CommonModule, NgFor } from '@angular/common';
import { Component, ChangeDetectorRef, OnInit} from '@angular/core';
import { ICandidatesSlider, candidate, CurrentCandidateService } from 'app/services/current-district.service';
import { FormsModule } from '@angular/forms';
import {ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-candidates-slider',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './candidates-slider.component.html',
  styleUrl: './candidates-slider.component.scss'
})
export class CandidatesSliderComponent implements OnInit{
  currentIndex: number = 0;
  district: any;
  text: any;
  age: any;
  name: any;
  currentCandidate: any;
  candidateSlider: ICandidatesSlider | null = null;
  candidateData: candidate[] | undefined;
  selectedDistrict: number | undefined;

  constructor(private router: Router, private ActivatedRoute: ActivatedRoute, public currentCandidateService: CurrentCandidateService) { }

  ngOnInit(): void { 
    this.ActivatedRoute.params.subscribe(params => { 
      const id = params['id']; 
      this.candidateSlider = this.currentCandidateService.getCandidate(); 
      this.candidateData = this.candidateSlider?.candidate; 
      this.router.navigate(['/current-district']);
    })
  };
  
  prevImage() {
    if(this.candidateData != undefined) {
      this.currentIndex =
      this.currentIndex === 0
      ? this.candidateData.length - 1
      : this.currentIndex - 1;
    }
    this.updateData();
  }

  nextImage() {
    if(this.candidateData != undefined) {
        this.currentIndex =
        this.currentIndex === this.candidateData.length - 1
          ? 0
          : this.currentIndex + 1;
    }
    this.updateData();
  }

  updateData() { 
    this.currentCandidateService.candidatesSlider.forEach(candidate => {
      this.name = candidate.candidate[this.currentIndex].candidateName; 
      this.age = candidate.candidate[this.currentIndex].candidateAge; 
      this.text = candidate.candidate[this.currentIndex].candidateText; 
    })
  } 

  toExit() {
    this.router.navigate(['']);
  }

  toggle(event: MouseEvent, district: number) {
    const items = document.querySelectorAll('.text');
    this.selectedDistrict = district;
    items.forEach((text) => {
      text.setAttribute('style', 'color: ##aba9a9')
    });
    (event.target as HTMLElement).style.color = '#000'
  }
}