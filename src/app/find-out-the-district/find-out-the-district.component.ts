import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpHeaders } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import {Observable, of, throwError} from "rxjs";
import { map, catchError} from "rxjs/operators";

@Component({
  selector: 'app-find-out-the-district',
  standalone: true,
  imports: [FormsModule,CommonModule],
  templateUrl: './find-out-the-district.component.html',
  styleUrl: './find-out-the-district.component.scss'
})

export class FindOutTheDistrictComponent { 
  constructor(private http: HttpClient) {} 
  input: string | undefined; 
  address: any; 
  street: string | undefined; 
  home: number | undefined; 
  housing: number | undefined 
  response: any; 
  isNotification: boolean = false; 
  errorMessage: string | null = null; 
  headers = new HttpHeaders().set('Server-Api-Token', '32cbdcdd-db17-482d-8324-edcc5e97afb5'); 
  find() { 
    if (this.input != undefined) { 
      this.address = this.input.split(','); 
      this.street = this.address[0]; 
      this.home = parseInt(this.address[1]); 
      this.housing = parseInt(this.address[2]); 
    } 
    if (this.housing != undefined && !Number.isNaN(this.housing) && this.street != '' && this.home != undefined) { 
      this.http.get(`https://kupchino-pistolalex888.amvera.io/api/v1/address?street=${this.street}&home=${this.home}&housing=${this.housing}`, {headers: this.headers}) 
      .pipe(
        catchError((err: HttpErrorResponse) => {
          console.error('HTTP Error', err);
          return of(err);
        })
      )
      .subscribe((response => { 
          this.response = response;  
          this.openPop(); 
          
        }))
      }
    else if (Number.isNaN(this.housing)) { 
      this.http.get(`https://kupchino-pistolalex888.amvera.io/api/v1/address?street=${this.street}&home=${this.home}`, {headers: this.headers}) 
      .subscribe((responce => { 
        this.response = responce; 
        this.openPop(); 
      })) 
    } else { 
      alert('Вы неправильно ввели адрес!') 
    } 
  }       
   
  openPop() { 
    this.isNotification = true; 
  } 

  toExit() {
    this.isNotification = false;
  }
}