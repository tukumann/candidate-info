import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-improvement-slider',
  standalone: true,
  imports: [],
  templateUrl: './improvement-slider.component.html',
  styleUrl: './improvement-slider.component.scss'
})
export class ImprovementSliderComponent {
  @Input() textArray!: any; 
  title: any;
  text: any;
  currentIndex: number = 0; 

  prevText() {
    if (this.textArray != undefined) {
      this.currentIndex =
        this.currentIndex === 0
          ? this.textArray.length - 1
          : this.currentIndex - 1;
      this.updateText();
    }
  }

  nextText() {
    if (this.textArray != undefined) {
      this.currentIndex =
        this.currentIndex === this.textArray.length - 1
          ? 0
          : this.currentIndex + 1;
      this.updateText();
    }
  }

  updateText() { 
    this.title = this.textArray[this.currentIndex].title; 
    this.text = this.textArray[this.currentIndex].text; 
  } 
}
