import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SendContactDataService } from '../services/send-contact-data.service';

@Component({
  selector: 'app-footer',
  standalone: true,
  imports: [FormsModule],
  templateUrl: './footer.component.html',
  styleUrl: './footer.component.scss',
  providers: [SendContactDataService],
})
export class FooterComponent {
  formData = {
    name: '',
    number: '',
    address: '',
  };

  constructor(public sendContactDataService: SendContactDataService) {}

  sendData() { 
    if (this.formData.address != '' && this.formData.name != '' && this.formData.number != '') {
      this.sendContactDataService.sendContactData(this.formData).subscribe(); 
      this.clear();
      alert('Спасибо! Данные успешно отправлены.')
    }
    else {
      alert('Вы не заполнили до конца форму. Форма не была отправлена.')
    }
  }

  clear() {
    this.formData = {
      name: '',
      number: '',
      address: '',
    };
  }
}
