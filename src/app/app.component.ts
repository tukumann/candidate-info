import { Component } from '@angular/core';
import { Router, RouterOutlet } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NavigationComponent } from './navigation/navigation.component';
import { CallToActionComponent } from './call-to-action/call-to-action.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { OurValuesComponent } from './our-values/our-values.component';
import { OurAchievementsComponent } from './our-achievements/our-achievements.component';
import { MapComponent } from './map/map.component';
import { FooterComponent } from './footer/footer.component';
import { FooterBottomComponent } from './footer-bottom/footer-bottom.component';
import { CandidatesComponent } from './candidates/candidates.component';
import { CandidatesSliderComponent } from './candidates-slider/candidates-slider.component';
import { ImprovedKupchinoComponent } from './improved-kupchino/improved-kupchino.component';
import { SecondOurAchievementsComponent } from './second-our-achievements/second-our-achievements.component';
import { FindOutTheDistrictComponent } from './find-out-the-district/find-out-the-district.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet, 
    CommonModule,
    NavigationComponent,
    CallToActionComponent,
    AboutUsComponent,
    OurValuesComponent,
    OurAchievementsComponent,
    MapComponent,
    FooterComponent,
    FooterBottomComponent,
    CandidatesComponent,
    CandidatesSliderComponent,
    ImprovedKupchinoComponent,
    SecondOurAchievementsComponent,
    FindOutTheDistrictComponent,
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'candidate-info';
  constructor(private router: Router) {}
  ngOnInit() {
    this.router.navigate([''])
  }
}
