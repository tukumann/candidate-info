import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { constants } from '../constants';

@Component({
  selector: 'app-navigation',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './navigation.component.html',
  styleUrl: './navigation.component.scss'
})
export class NavigationComponent {
  isOpenMenu: boolean | undefined = false;
  toggleOpen() {
    this.isOpenMenu = !this.isOpenMenu
  }
  tg = constants.TG
  vk = constants.VK
}
