import { AfterContentInit, Component, Input, OnChanges, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OurAchievementsScrollComponent } from '../slider/slider.component';
import { constants } from './constants';

interface ISliderImage {
  src: string;
  alt: string;
}

@Component({
  selector: 'app-our-achievements',
  standalone: true,
  imports: [OurAchievementsScrollComponent, CommonModule],
  templateUrl: './our-achievements.component.html',
  styleUrl: './our-achievements.component.scss'
})
export class OurAchievementsComponent {
  imgsArray1: ISliderImage[] | undefined;
  imgsArray2: ISliderImage[] | undefined;
  imgsArray3: ISliderImage[] | undefined;
  imgsArray4: ISliderImage[] | undefined;
  imgsArray5: ISliderImage[] | undefined;
  imgsArray6: ISliderImage[] | undefined;
  imgsArray7: ISliderImage[] | undefined;
  imgsArray8: ISliderImage[] | undefined;
  imgsArray9: ISliderImage[] | undefined;
  @Input() src: any;
  constants = constants;
}


