export const constants = {
    imgsOurAchievements1: [
    {
      src: '/our-achievements/save-park/img-1.jpg',
      alt: 'img-1',
    },
    {
      src: '/our-achievements/save-park/img-2.jpg',
      alt: 'img-2',
    },
    {
      src: '/our-achievements/save-park/img-3.jpg',
      alt: 'img-3',
    }
  ],

  imgsOurAchievements2: [
    {
      src: '/our-achievements/events/img-1.jpg',
      alt: 'img-1',
    },
    {
      src: '/our-achievements/events/img-2.jpg',
      alt: 'img-1',
    },
    {
      src: '/our-achievements/events/img-3.jpg',
      alt: 'img-2',
    },
    {
      src: '/our-achievements/events/img-4.jpg',
      alt: 'img-3',
    },
    {
      src: '/our-achievements/events/img-5.jpeg',
      alt: 'img-4',
    }
  ],

  imgsOurAchievements3: [
    {
      src: '/our-achievements/memory-area/img-1.jpg',
      alt: 'img-1',
    },
    {
      src: '/our-achievements/memory-area/img-2.jpg',
      alt: 'img-2',
    }
  ],

  imgsOurAchievements4: [
    {
      src: '/our-achievements/no-car/img-1.jpg',
      alt: 'img-1',
    },
    {
      src: '/our-achievements/no-car/img-2.jpg',
      alt: 'img-2',
    }
  ],

  imgsOurAchievements5: [
    {
      src: '/our-achievements/no-krt/img-1.jpg',
      alt: 'img-1',
    },
    {
      src: '/our-achievements/no-krt/img-2.jpg',
      alt: 'img-2',
    }
  ],

  imgsOurAchievements6: [
    {
      src: '/our-achievements/playgrounds/img-1.jpg',
      alt: 'img-1',
    },
    {
      src: '/our-achievements/playgrounds/img-2.jpg',
      alt: 'img-2',
    },
    {
      src: '/our-achievements/playgrounds/img-3.jpg',
      alt: 'img-3',
    },
    {
      src: '/our-achievements/playgrounds/img-4.jpg',
      alt: 'img-4',
    },
    {
      src: '/our-achievements/playgrounds/img-5.jpg',
      alt: 'img-5',
    }
  ],

  imgsOurAchievements7: [
    {
      src: '/our-achievements/relax-area/img-1.jpg',
      alt: 'img-1',
    },
    {
      src: '/our-achievements/relax-area/img-2.jpg',
      alt: 'img-2',
    },
    {
      src: '/our-achievements/relax-area/img-3.jpg',
      alt: 'img-3',
    },
    {
      src: '/our-achievements/relax-area/img-4.jpg',
      alt: 'img-4',
    }
  ],

  imgsOurAchievements8: [
    {
      src: '/our-achievements/youth-council/img-1.jpg',
      alt: 'img-1',
    },
    {
      src: '/our-achievements/youth-council/img-2.jpg',
      alt: 'img-2',
    },
    {
      src: '/our-achievements/youth-council/img-3.jpg',
      alt: 'img-3',
    },
    {
      src: '/our-achievements/youth-council/img-4.jpg',
      alt: 'img-4',
    }
  ],

  imgsOurAchievements9: [
    {
      src: '/our-achievements/roads/img-1.jpg',
      alt: 'img-1',
    },
    {
      src: '/our-achievements/roads/img-2.jpg',
      alt: 'img-2',
    },
    {
      src: '/our-achievements/roads/img-3.jpg',
      alt: 'img-3',
    },
    {
      src: '/our-achievements/roads/img-4.jpg',
      alt: 'img-4',
    },
    {
      src: '/our-achievements/roads/img-5.jpg',
      alt: 'img-5',
    }
  ]
}