import { Component, OnInit } from '@angular/core';
import { OurAchievementsScrollComponent } from 'app/slider/slider.component';
import { constants } from './constants';

interface ISliderImage {
  src: string;
  alt: string;
}

@Component({
  selector: 'app-second-our-achievements',
  standalone: true,
  imports: [OurAchievementsScrollComponent],
  templateUrl: './second-our-achievements.component.html',
  styleUrl: './second-our-achievements.component.scss'
})

export class SecondOurAchievementsComponent {
  imgsArray1: ISliderImage[] | undefined;
  imgsArray2: ISliderImage[] | undefined;
  imgsArray3: ISliderImage[] | undefined;
  imgsArray4: ISliderImage[] | undefined;
  constants = constants
}
