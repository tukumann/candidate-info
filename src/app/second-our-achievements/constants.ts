export const constants = {
    imgsSecondOurAchievements1: [
    {
        src: '/second-our-achievements/no-krt/img-1.jpg',
        alt: 'img-1'
    },
    {
        src: '/second-our-achievements/no-krt/img-2.png',
        alt: 'img-2'
    },
    ],

    imgsSecondOurAchievements2: [
    {
        src: '/second-our-achievements/sit-place/img-1.png',
        alt: 'img-1'
    },
    {
        src: '/second-our-achievements/sit-place/img-2.jpg',
        alt: 'img-2'
    },
    ],
    
    imgsSecondOurAchievements3: [
    {
        src: '/second-our-achievements/keep-place/img-1.jpg',
        alt: 'img-1'
    },
    {
        src: '/second-our-achievements/keep-place/img-2.png',
        alt: 'img-2'
    },
    ],

    imgsSecondOurAchievements4: [
    {
        src: '/second-our-achievements/events/img-1.png',
        alt: 'img-1'
    },
    {
        src: '/second-our-achievements/events/img-2.png',
        alt: 'img-2'
    },
    {
        src: '/second-our-achievements/events/img-3.png',
        alt: 'img-3'
    }
    ]
}