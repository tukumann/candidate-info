import { Component, OnInit } from '@angular/core';
import { ImprovementSliderComponent } from 'app/improvement-slider/improvement-slider.component';
import { constants } from './constants';

interface ISliderText {
  title: string;
  text: string;
}

@Component({
  selector: 'app-improved-kupchino',
  standalone: true,
  imports: [ImprovementSliderComponent],
  templateUrl: './improved-kupchino.component.html',
  styleUrl: './improved-kupchino.component.scss'
})
export class ImprovedKupchinoComponent {
  textArray: ISliderText[] | undefined;
  constants = constants;
}
