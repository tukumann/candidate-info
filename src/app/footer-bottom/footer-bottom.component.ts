import { Component } from '@angular/core';
import { constants } from '../constants';

@Component({
  selector: 'app-footer-bottom',
  standalone: true,
  templateUrl: './footer-bottom.component.html',
  styleUrl: './footer-bottom.component.scss'
})
export class FooterBottomComponent {
  tg = constants.TG
  vk = constants.VK
}
