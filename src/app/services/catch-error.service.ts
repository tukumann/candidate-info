import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        let errorMessage = '';

        if (error.error instanceof ErrorEvent) {
          // Клиентская ошибка
          errorMessage = `Ошибка: ${error.error.message}`;
        } else {
          // Серверная ошибка
          errorMessage = error.error.message || `Код ошибки: ${error.status}, сообщение: ${error.message}`;
        }

        // Здесь вы можете передать сообщение об ошибке в сервис или компонент
        return throwError(() => new Error(errorMessage))
      })
    );
  }
}
