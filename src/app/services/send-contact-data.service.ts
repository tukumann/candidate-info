import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SendContactDataService { 
  constructor(private http: HttpClient) { } 

  api: string = 'https://kupchino-pistolalex888.amvera.io/'; 
  
  sendContactData(data: any) {  
    const headers = new HttpHeaders().set('Server-Api-Token', '32cbdcdd-db17-482d-8324-edcc5e97afb5'); 
    return this.http.post(`${this.api}api/v1/feedback`, data, { headers });  
  }
}
