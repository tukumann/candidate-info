import { CommonModule } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { RouterOutlet, RouterLink, RouterLinkActive, Router, ActivatedRoute } from '@angular/router';
import { ICandidatesSlider } from '../services/current-district.service';
import { CurrentCandidateService } from 'app/services/current-district.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-candidates',
  standalone: true,
  imports: [RouterOutlet, RouterLink, RouterLinkActive, CommonModule],
  templateUrl: './candidates.component.html',
  styleUrl: './candidates.component.scss'
})

export class CandidatesComponent {
  constructor(public currentCandidateService: CurrentCandidateService, private router: Router, private activeRoute: ActivatedRoute) {
  }
  
  currentCandidate: ICandidatesSlider | null = {id: '', district: 0, candidate: []};
  openDistrict(district: ICandidatesSlider) { 
    this.currentCandidateService.setCandidate(district); 
    this.currentCandidate = this.currentCandidateService.getCandidate(); 
  }
}
