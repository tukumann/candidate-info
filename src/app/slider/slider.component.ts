import { AfterViewInit, Component, Input, OnChanges, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-slider',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './slider.component.html',
  styleUrl: './slider.component.scss',
})
export class OurAchievementsScrollComponent{
  @Input() imgsArray!: any; 
  alt: any;
  src: any;
  currentIndex: number = 0; 

  prevImage() {
    if (this.imgsArray != undefined) {
      this.currentIndex =
        this.currentIndex === 0
          ? this.imgsArray.length - 1
          : this.currentIndex - 1;
      this.updateImage();
    }
  }

  nextImage() {
    if (this.imgsArray != undefined) {
      this.currentIndex =
        this.currentIndex === this.imgsArray.length - 1
          ? 0
          : this.currentIndex + 1;
      this.updateImage();
    }
  }

  updateImage() { 
    this.alt = this.imgsArray[this.currentIndex].alt; 
    this.src = this.imgsArray[this.currentIndex].src; 
  } 
}
